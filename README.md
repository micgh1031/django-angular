# django-angularjs job demo app

## Installation

* `$ git clone https://micgh1031@bitbucket.org/micgh1031/django-angular.git`
* `$ mkvirtualenv djangularangular`
* `$ cd django-angular/`
* `$ pip install -r requirements.txt`
* `$ npm install`
* `$ bower install`
* `$ python manage.py migrate`
* `$ python manage.py runserver`

## Deployment

*NOTE: Requires [Heroku Toolbelt](https://toolbelt.heroku.com/).*

* `$ heroku apps:create`
* `$ heroku config:set BUILDPACK_URL=https://github.com/ddollar/heroku-buildpack-multi.git`
* `$ heroku config:set DEBUG=False`
* `$ heroku config:set COMPRESS_ENABLED=True`
* `$ git push heroku master`
* `$ heroku open`

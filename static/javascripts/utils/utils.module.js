(function () {
  'use strict';

  angular
    .module('job_demo.utils', [
      'job_demo.utils.services'
    ]);

  angular
    .module('job_demo.utils.services', []);
})();

(function () {
  'use strict';

  angular
    .module('job_demo.accounts.controllers')
    .controller('AccountController', AccountController);

  AccountController.$inject = ['$location', '$routeParams', 'Posts', 'Account', 'Snackbar'];

  function AccountController($location, $routeParams, Posts, Account, Snackbar) {
    var vm = this;

    vm.account = undefined;
    vm.posts = [];

    activate();

    function activate() {
      var username = $routeParams.username.substr(1);

      Account.get(username).then(accountSuccessFn, accountErrorFn);
      Posts.get(username).then(postsSuccessFn, postsErrorFn);

      function accountSuccessFn(data, status, headers, config) {
        vm.account = data.data;
      }

      function accountErrorFn(data, status, headers, config) {
        $location.url('/');
        Snackbar.error('That user does not exist.');
      }

      function postsSuccessFn(data, status, headers, config) {
        vm.posts = data.data;
      }

      function postsErrorFn(data, status, headers, config) {
        Snackbar.error(data.data.error);
      }
    }
  }
})();

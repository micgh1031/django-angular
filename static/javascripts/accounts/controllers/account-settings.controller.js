(function () {
  'use strict';

  angular
    .module('job_demo.accounts.controllers')
    .controller('AccountSettingsController', AccountSettingsController);

  AccountSettingsController.$inject = [
    '$location', '$routeParams', 'Authentication', 'Account', 'Snackbar'
  ];

  function AccountSettingsController($location, $routeParams, Authentication, Account, Snackbar) {
    var vm = this;

    vm.destroy = destroy;
    vm.update = update;

    activate();

    function activate() {
      var authenticatedAccount = Authentication.getAuthenticatedAccount();
      var username = $routeParams.username.substr(1);

      // Redirect if not logged in
      if (!authenticatedAccount) {
        $location.url('/');
        Snackbar.error('You are not authorized to view this page.');
      } else {
        // Redirect if logged in, but not the owner of this account.
        if (authenticatedAccount.username !== username) {
          debugger;
          $location.url('/');
          Snackbar.error('You are not authorized to view this page.');
        }
      }

      Account.get(username).then(accountSuccessFn, accountErrorFn);

      function accountSuccessFn(data, status, headers, config) {
        vm.account = data.data;
      }

      function accountErrorFn(data, status, headers, config) {
        $location.url('/');
        Snackbar.error('That user does not exist.');
      }
    }

    function destroy() {
      Account.destroy(vm.account.username).then(accountSuccessFn, accountErrorFn);

      function accountSuccessFn(data, status, headers, config) {
        Authentication.unauthenticate();
        window.location = '/';

        Snackbar.show('Your account has been deleted.');
      }

      function accountErrorFn(data, status, headers, config) {
        Snackbar.error(data.error);
      }
    }

    function update() {
      var username = $routeParams.username.substr(1);

      Account.update(username, vm.account).then(accountSuccessFn, accountErrorFn);

      function accountSuccessFn(data, status, headers, config) {
        Snackbar.show('Your account has been updated.');
      }

      function accountErrorFn(data, status, headers, config) {
        Snackbar.error(data.error);
      }
    }
  }
})();

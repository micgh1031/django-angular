(function () {
  'use strict';

  angular
    .module('job_demo.accounts', [
      'job_demo.accounts.controllers',
      'job_demo.accounts.services'
    ]);

  angular
    .module('job_demo.accounts.controllers', []);

  angular
    .module('job_demo.accounts.services', []);
})();

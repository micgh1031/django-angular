(function () {
  'use strict';

  angular
    .module('job_demo.authentication', [
      'job_demo.authentication.controllers',
      'job_demo.authentication.services'
    ]);

  angular
    .module('job_demo.authentication.controllers', []);

  angular
    .module('job_demo.authentication.services', ['ngCookies']);
})();

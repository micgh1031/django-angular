(function () {
  'use strict';

  angular
    .module('job_demo.posts', [
      'job_demo.posts.controllers',
      'job_demo.posts.directives',
      'job_demo.posts.services'
    ]);

  angular
    .module('job_demo.posts.controllers', []);

  angular
    .module('job_demo.posts.directives', ['ngDialog']);

  angular
    .module('job_demo.posts.services', []);
})();

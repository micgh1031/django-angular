(function () {
  'use strict';

  angular
    .module('job_demo', [
      'job_demo.config',
      'job_demo.routes',
      'job_demo.accounts',
      'job_demo.authentication',
      'job_demo.layout',
      'job_demo.posts',
      'job_demo.utils'
    ]);

  angular
    .module('job_demo.config', []);

  angular
    .module('job_demo.routes', ['ngRoute']);

  angular
    .module('job_demo')
    .run(run);

  run.$inject = ['$http'];

  function run($http) {
    $http.defaults.xsrfHeaderName = 'X-CSRFToken';
    $http.defaults.xsrfCookieName = 'csrftoken';
  }
})();

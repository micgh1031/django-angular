(function () {
  'use strict';

  angular
    .module('job_demo.layout', [
      'job_demo.layout.controllers'
    ]);

  angular
    .module('job_demo.layout.controllers', []);
})();
